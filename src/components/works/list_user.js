import {
  Table,
  Tag,
  Space,
  Button,
  List,
  Timeline,
  Collapse,
  Typography,
  Drawer,
} from "antd"
import CollapsePanel from "antd/lib/collapse/CollapsePanel"
import { useState } from "react"
import ApplyWork from "./apply_work"
import Link from "next/link"
const works = require("src/data/request_work.json")

const UserWorks = () => {
  const [visible, setVisible] = useState(false)
  const showDrawer = () => {
    setVisible(true)
  }
  const onClose = () => {
    setVisible(false)
  }
  const columns = [
    {
      title: "Дугаар",
      dataIndex: "id",
      key: "id",
      render: (id) => <a>{id}</a>,
    },
    {
      title: "Нэршил",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link href={`/work/${record?.id}`}>
          <a>{text}</a>
        </Link>
      ),
    },
    {
      title: "Тавигдах шаардлага",
      dataIndex: "requirements",
      key: "requirements",
      render: (requirements, record) => (
        <>
          <List
            size="small"
            dataSource={requirements}
            renderItem={(item) => <List.Item>{item}</List.Item>}
          />
          {record?.license.length > 0 ? (
            <Collapse>
              <CollapsePanel header="Шаадлагатай зөвшөөрөл">
                <List size="small">
                  {record?.license.map((item, id) => (
                    <List.Item key={id}>{item}</List.Item>
                  ))}
                </List>
              </CollapsePanel>
            </Collapse>
          ) : (
            ""
          )}
        </>
      ),
    },

    {
      title: "Статус",
      key: "status",
      dataIndex: "status",
      render: (status) => (
        <Tag color={status === "энгийн" ? "geekblue" : "orange"} key={status}>
          {status?.toUpperCase()}
        </Tag>
      ),
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <Button type="link" onClick={showDrawer}>
          Оролцох
        </Button>
      ),
    },
  ]
  return (
    <>
      <Table dataSource={works} columns={columns} />
      <Drawer
        title="Тендерт оролцох"
        placement="right"
        onClose={onClose}
        visible={visible}
        size="large"
        width={"50%"}
      >
        <ApplyWork />
      </Drawer>
    </>
  )
}

export default UserWorks
