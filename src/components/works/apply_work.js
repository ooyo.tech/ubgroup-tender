import { Form, Select, InputNumber, Input, Button, Upload, Space, DatePicker, Radio } from "antd"
import { UploadOutlined } from "@ant-design/icons"
const works = require("src/data/request_work.json")
const { Option } = Select
const { RangePicker } = DatePicker
 

const normFile = (e) => {
  console.log("Upload event:", e)

  if (Array.isArray(e)) {
    return e
  }

  return e && e.fileList
}

const ApplyWork = () => {
  const onFinish = (values) => {
    //   const
    console.log("Received values of form: ", values)
  }

  return (
    <Form name="validate_other" layout="vertical" onFinish={onFinish}>
       <Form.Item name="select" label="Гүйцэтгэх ажил сонгох" hasFeedback>
          <Select style={{ width: "100%" }}>
              {works.map(item => <Option value={item?.id} key={item?.id}>{item?.name}</Option> )}
            
          </Select>
      </Form.Item> 
      <Form.Item label="Материал хавсаргах">
      <Upload name="logo" action="/upload.do" listType="picture">
          <Button icon={<UploadOutlined />}>Энд дарна уу</Button>
        </Upload>
      </Form.Item>      
      <Form.Item name="testPaper" label="Дээрхи бүх нөхцөлийг хангах эсэх">
        <Radio.Group>
         <Space>
         <Radio.Button value="a">Хангана</Radio.Button>
          <Radio.Button value="b">Хангахгүй</Radio.Button>
         </Space>
        </Radio.Group>
      </Form.Item>
      <Form.Item label="Нэмэлт тайлбар">
          <Input.TextArea/>
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 12,
          offset: 6,
        }}
      >
        <Button type="primary" htmlType="submit">
          Илгээх
        </Button>
      </Form.Item>
    </Form>
  )
}

export default ApplyWork
