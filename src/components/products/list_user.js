import { Table, Tag, Space, Typography, Button, Drawer } from "antd"
import { useState } from "react"
import ApplyProduct from "./apply_product"
import Link from "next/link"

const UserProducts = ({ data }) => {
  const [visible, setVisible] = useState(false)
  const showDrawer = () => {
    setVisible(true)
  }
  const onClose = () => {
    setVisible(false)
  }
  const columns = [
    {
      title: "Дугаар",
      dataIndex: "id",
      key: "id",
      render: (id) => <a>{id}</a>,
    },
    {
      title: "Нэршил",
      dataIndex: "name",
      key: "name",
      render: (text, record) => (
        <Link href={`/product/${record?.id}`}>
          <a>{text}</a>
        </Link>
      ),
    },
    {
      title: "Харьцаа",
      dataIndex: "dimension",
      key: "dimension",
    },
    {
      title: "Ширхэг, хэмжээ",
      dataIndex: "pieces",
      key: "pieces",
      render: (piece, record) => {
        return (
          <Typography.Text>
            {record?.pieces} {record?.unitOfMeasure}
          </Typography.Text>
        )
      },
    },
    {
      title: "Нэмэлт тайлбар",
      key: "description",
      dataIndex: "description",
    },
    {
      title: "Статус",
      key: "status",
      dataIndex: "status",
      render: (status) => (
        <Tag color={status === "энгийн" ? "geekblue" : "orange"} key={status}>
          {status?.toUpperCase()}
        </Tag>
      ),
    },
    {
      title: "",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Button type="link" onClick={showDrawer}>
            Оролцох
          </Button>
        </Space>
      ),
    },
  ]
  return (
    <>
      <Table dataSource={data} columns={columns} />
      <Drawer
        title="Тендерт оролцох"
        placement="right"
        onClose={onClose}
        visible={visible}
        size="large"
        width={"50%"}
      >
        <ApplyProduct />
      </Drawer>
    </>
  )
}

export default UserProducts
