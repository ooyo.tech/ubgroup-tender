import { Form, Select, InputNumber, Input, Button, Upload, Space, DatePicker } from "antd"
import { UploadOutlined } from "@ant-design/icons"
const { Option } = Select
const { RangePicker } = DatePicker
const formItemLayout = {
  labelCol: {
    span: 6,
  },
  wrapperCol: {
    span: 14,
  },
}

const normFile = (e) => {
  console.log("Upload event:", e)

  if (Array.isArray(e)) {
    return e
  }

  return e && e.fileList
}

const AddProduct = () => {
  const onFinish = (values) => {
    //   const
    console.log("Received values of form: ", values)
  }

  return (
    <Form name="validate_other" {...formItemLayout} onFinish={onFinish}>
      <Form.Item name={"name"} label="Нэршил">
        <Input />
      </Form.Item>
      <Form.Item name="select" label="Хэмжээ" hasFeedback>
        <Space>
          <InputNumber min={1} max={10} style={{ width: "100%" }} />
          <Select style={{ width: 100 }}>
            <Option value="peices">Ширхэг</Option>
            <Option value="mk">МКВ</Option>
          </Select>
        </Space>
      </Form.Item>
      <Form.Item name={"introduction"} label="Тайлбар">
        <Input.TextArea />
      </Form.Item>
      <Form.Item
        name="upload"
        label="Зураг оруулах"
        valuePropName="fileList"
        getValueFromEvent={normFile}
      >
        <Upload name="logo" action="/upload.do" listType="picture">
          <Button icon={<UploadOutlined />}>Click to upload</Button>
        </Upload>
      </Form.Item>

      <Form.Item
        name="upload"
        label="Нэмэлт файл оруулах"
        valuePropName="fileList"
        getValueFromEvent={normFile}
      >
        <Upload name="logo" action="/upload.do" listType="picture">
          <Button icon={<UploadOutlined />}>Click to upload</Button>
        </Upload>
      </Form.Item>
      <Form.Item name="duration" label="Хугацаа">
        <RangePicker />
      </Form.Item>
      <Form.Item name="status" label="Төлөв">
        <Select style={{ width: 120 }}>
          <Option value="lucy">Энгийн</Option>
          <Option value="lucy">Яаралтай</Option>
        </Select>
      </Form.Item>

      <Form.Item
        wrapperCol={{
          span: 12,
          offset: 6,
        }}
      >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  )
}

export default AddProduct
