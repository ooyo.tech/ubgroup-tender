import {
  Form,
  Select,
  InputNumber,
  Input,
  Button,
  Upload,
  Space,
  DatePicker,
  Radio,
} from "antd"
import { UploadOutlined } from "@ant-design/icons"
const products = require("src/data/request_product.json")
const { Option } = Select
const { RangePicker } = DatePicker
const formItemLayout = {
  labelCol: {
    span: 18,
  },
  wrapperCol: {
    span: 18,
  },
}

const normFile = (e) => {
  console.log("Upload event:", e)

  if (Array.isArray(e)) {
    return e
  }

  return e && e.fileList
}

const ApplyProduct = () => {
  const onFinish = (values) => {
    //   const
    console.log("Received values of form: ", values)
  }

  return (
    <Form name="validate_other" {...formItemLayout} layout="vertical" onFinish={onFinish}>
      <Form.Item name="select" label="Бүтээгдэхүүн сонгох" hasFeedback>
        <Select style={{ width: "100%" }}>
          {products.map((item) => (
            // eslint-disable-next-line react/jsx-key
            <Option value={item?.id}>{item?.name}</Option>
          ))}
        </Select>
      </Form.Item>
      <Form.Item name="testPaper" label="Шинжилгээний бичиг">
        <Radio.Group>
          <Space>
            <Radio.Button value="a">Бичигтэй</Radio.Button>
            <Radio.Button value="b">Бичиггүй</Radio.Button>
          </Space>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        name="upload"
        label="Бүтээгдэхүүний зураг оруулах"
        valuePropName="fileList"
        getValueFromEvent={normFile}
      >
        <Upload name="logo" action="/upload.do" listType="picture">
          <Button icon={<UploadOutlined />}>Энд дарна уу</Button>
        </Upload>
      </Form.Item>
      <Form.Item name={"pricePiece"} label="Нэгж үнэ">
        <Space>
          <InputNumber min={1} max={10} style={{ width: "100%" }} />
          <Select style={{ width: 100 }}>
            <Option value="peices">Ширхэг</Option>
            <Option value="mk">МКВ</Option>
            <Option value="mk">м3</Option>
            <Option value="mk">м</Option>
          </Select>
        </Space>
      </Form.Item>
      <Form.Item label="Нийт үнэ">
        <Input />
      </Form.Item>
      <Form.Item name="duration" label="Нийлүүлэх хугацаа">
        <RangePicker />
      </Form.Item>
      <Form.Item label="Үйлдвэрлэсэн улс">
        <Input />
      </Form.Item>
      <Form.Item label="Ижил чанартай үнэ бага бүтээгдэхүүн санал болгох">
        <Upload name="logo" action="/upload.do" listType="picture">
          <Button icon={<UploadOutlined />}>Энд дарна уу</Button>
        </Upload>
      </Form.Item>
      <Form.Item label="Өмнө нийлүүлж байсан төслүүд">
        <Upload name="logo" action="/upload.do" listType="picture">
          <Button icon={<UploadOutlined />}>Энд дарна уу</Button>
        </Upload>
      </Form.Item>
      <Form.Item
        name="testPaper"
        label="Ижил төрлийн бараа материал нийлүүлж байсан туршлага"
      >
        <Input.TextArea />
      </Form.Item>
      <Form.Item name="testPaper" label="Санхүүгийн чадавхи үнийн дүнгийн 50%-тай тэнцэх">
        <Radio.Group>
          <Space>
            <Radio.Button value="a">Тэнцэнэ</Radio.Button>
            <Radio.Button value="b">Тэнцэхгүй</Radio.Button>
          </Space>
        </Radio.Group>
      </Form.Item>
      <Form.Item
        wrapperCol={{
          span: 12,
          offset: 6,
        }}
      >
        <Button type="primary" htmlType="submit">
          Илгээх
        </Button>
      </Form.Item>
    </Form>
  )
}

export default ApplyProduct
