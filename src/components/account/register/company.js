import { Form, Input, InputNumber, Button, Space } from "antd"
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 24,
  },
}
/* eslint-disable no-template-curly-in-string */

const CompanyRegister = () => {
  const onFinish = (values) => {
    console.log(values)
  }

  return (
    <Form {...layout} name="nest-messages" onFinish={onFinish} layout="vertical">
      <Form.Item name={["user", "name"]} label="Компани Нэр">
        <Input />
      </Form.Item>
      <Form.Item name={["register"]} label="Улсын бүртгэл дугаар">
        <Input />
      </Form.Item>
      <Form.Item name={["phone"]} label="Утас">
        <InputNumber style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item name={["experienceDuration"]} label="Үйл ажиллаагаа эрхэлсэн жил">
        <Input />
      </Form.Item>
      <Form.Item name={["introduction"]} label="Товч танилцуулга">
        <Input.TextArea />
      </Form.Item>
      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
        <Space>
          <Button type="primary" href="/">
            Илгээх
          </Button>
          <Button type="default" href="/">
            Буцах
          </Button>
        </Space>
      </Form.Item>
    </Form>
  )
}

export default CompanyRegister
