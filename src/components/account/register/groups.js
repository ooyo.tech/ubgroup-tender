import { Form, Input, InputNumber, Button, Space } from "antd"
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 24,
  },
}

const GroupRegister = () => {
  const onFinish = (values) => {
    console.log(values)
  }

  return (
    <Form {...layout} name="nest-messages" layout="vertical" onFinish={onFinish}>
      <Form.Item name={["user", "name"]} label="Бригад Нэр">
        <Input />
      </Form.Item>
      <Form.Item name={["register"]} label="Регистр">
        <Input />
      </Form.Item>
      <Form.Item name={["phone"]} label="Утас">
        <InputNumber style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item name={["noOfUsers"]} label="Нийт гишүүн тоо">
        <InputNumber style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item name={["experienceDuration"]} label="Үйл ажиллаагаа эрхэлсэн жил">
        <Input />
      </Form.Item>
      <Form.Item name={["introduction"]} label="Товч танилцуулга">
        <Input.TextArea />
      </Form.Item>
      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
        <Space>
          <Button type="primary" href="/">
            Илгээх
          </Button>
          <Button type="default" href="/">
            Буцах
          </Button>
        </Space>
      </Form.Item>
    </Form>
  )
}
export default GroupRegister
