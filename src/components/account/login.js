import { Form, Input, Button, Checkbox, Space } from "antd"

const LoginComponent = () => {
  const onFinish = (values) => {
    console.log("Success:", values)
  }

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo)
  }

  return (
    <Form
      name="basic"
      labelCol={{
        span: 8,
      }}
      wrapperCol={{
        span: 16,
      }}
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
    >
      <Form.Item label="Нэвтрэх нэр" name="username">
        <Input />
      </Form.Item>

      <Form.Item label="Нууц үг" name="password">
        <Input.Password />
      </Form.Item>

      <Form.Item
        wrapperCol={{
          offset: 8,
          span: 16,
        }}
      >
        <Space>
          <Button type="primary" htmlType="submit">
            Нэвтрэх
          </Button>
          <Button type="default" href="/register">
            Бүртгүүлэх
          </Button>
        </Space>
      </Form.Item>
    </Form>
  )
}

export default LoginComponent
