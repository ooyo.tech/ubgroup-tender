import { Layout, Menu, Avatar, Typography } from "antd"
import UserProducts from "src/components/products/list_user"
import UserWorks from "src/components/works/list_user"
import Link from "next/link"
import AddProduct from "src/components/products/add_product" 
const products = require("src/data/request_product.json")
const { Content, Header, Footer } = Layout
export default function Home() {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Layout.Header style={{ padding: "0 20px" }}>
        <Typography.Text strong style={{ fontSize: "18px" }} className="hide-on-mobile">
          <Link href="/">UB GROUP</Link>
        </Typography.Text>

        <Menu mode="horizontal" style={{ float: "right", textAlign: "right" }}>
          <Menu.Item key="admin" href="/admin">
            Админ
          </Menu.Item>
        </Menu>
      </Layout.Header>
      <Content style={{ margin: "24px 16px 24px" }}>
       <AddProduct/>
       <UserWorks/>
      </Content>
      <Footer>Footer</Footer>
    </Layout>
  )
}
