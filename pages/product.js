import { Layout, Menu, Avatar, Typography, Row, Col, Card, Divider } from "antd"
import UserProducts from "src/components/products/list_user"
import UserWorks from "src/components/works/list_user"
import Link from "next/link"
import { FileAddOutlined } from '@ant-design/icons';
import AddProduct from "src/components/products/add_product";
const { SubMenu } = Menu;

const { Content, Header, Footer } = Layout
const products = require("src/data/request_product.json")
export default function Home() {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Layout.Header style={{ padding: "0 20px" }}>
        <Typography.Text strong style={{ fontSize: "18px" }} className="hide-on-mobile">
          <Link href="/">UB GROUP</Link>
        </Typography.Text>

        <Menu mode="horizontal" style={{ float: "right", textAlign: "right" }} theme="dark"> 
          <Menu.Item key="login" >
           <a href='login'> Нэвтрэх</a>
          </Menu.Item>
          <Menu.Item key="register">            
            <a href='register'> Бүртгүүлэх</a>
          </Menu.Item>  
                    <Menu.Item key="product">            
            <a href='product'> Гүйцэтгэлийн ажил захиалга</a>
          </Menu.Item>  
        </Menu>
      </Layout.Header>
      <Content style={{ margin: "24px 16px 24px" }}>
        <Row>
          <Col span={16} offset={4}>
          <Card title="Бүтээгдэхүүн захиалга шаардлага">
            <AddProduct/>
            </Card> 
        </Col> 
        </Row>
      </Content>
      <Footer>Copyright © 2021</Footer>
    </Layout>
  )
}
