import { Col, Layout, Row, Table } from "antd"
import LoginComponent from "src/components/account/login"

const { Content, Header, Footer } = Layout

export default function Home() {
  return (
    <Row justify="center" align="center" style={{ height: "100vh" }}>
      <Col
        xs={24}
        sm={24}
        md={8}
        lg={8}
        xl={8}
        style={{ display: "flex", flexDirection: "column", justifyContent: "center" }}
      >
        <LoginComponent />
      </Col>
    </Row>
  )
}
