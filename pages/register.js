import React, { useState } from "react"
import { Form, Input, Button, Radio, Row, Col, Space, Divider } from "antd"
import UserRegister from "src/components/account/register/user"
import GroupRegister from "src/components/account/register/groups"
import CompanyRegister from "src/components/account/register/company"

const RegisterScreen = () => {
  const [userSelection, setUserSelection] = useState("person")
  const handleGroupChange = (e) => {
    setUserSelection(e.target.value)
    console.log(`radio checked:${e.target.value}`)
  }

  return (
    <Row justify="center" align="center" style={{ height: "100vh" }}>
      <Col
        xs={24}
        sm={24}
        md={8}
        lg={8}
        xl={8}
        style={{ display: "flex", flexDirection: "column", justifyContent: "center" }}
      >
        <Radio.Group onChange={handleGroupChange} defaultValue={userSelection}>
          <Space>
            <Radio.Button value="person">Хувь хүн</Radio.Button>
            <Radio.Button value="group">Бригад</Radio.Button>
            <Radio.Button value="company">Албан байгууллага</Radio.Button>
          </Space>
        </Radio.Group>
      <Divider/>
        {userSelection === "person" ? (
          <UserRegister />
        ) : userSelection === "group" ? (
          <GroupRegister />
        ) : (
          <CompanyRegister />
        )}
      </Col>
    </Row>
  )
}

export default RegisterScreen
